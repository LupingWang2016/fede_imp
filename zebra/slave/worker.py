#!/usr/bin/env python
from __future__ import absolute_import, print_function, division

import os
import sys
import socket
import errno

import zope
from twisted.internet import reactor, endpoints, task, utils, interfaces, udp
from twisted.internet.protocol import ClientFactory, DatagramProtocol
from twisted.protocols.basic import NetstringReceiver

from zebra.msg import zebra_pb2
from zebra.helper import logging, parse_hosts_rev, ZERO
from zebra.slave.block_sender import BlockSender

pwd = os.path.dirname(os.path.abspath(__file__))
tc_path = os.path.join(pwd, "tc.sh")
hostname_to_ip = parse_hosts_rev()

min_send_rate = 8*1024*20


# https://twistedmatrix.com/trac/ticket/2790
# we monkey patch the udp.Port
def doWrite(self):
    # logging.debug("do write called.")
    if self.producer and self.producer.paused:
        self.producer.resumeProducing()
udp.Port.doWrite = doWrite


# def unregisterProducer(self):
#     import ipdb; ipdb.set_trace()
#     pass
# udp.Port.unregisterProducer = unregisterProducer

def connectionLost(self, reason=None):
    # import ipdb; ipdb.set_trace()
    pass
udp.Port.connectionLost = connectionLost


class DataHouseSender(DatagramProtocol):

    class Producer(object):
        zope.interface.implements(interfaces.IPushProducer)

        data_block_size = 1024
        data_block = 'H'*data_block_size

        def __init__(self, proto):
            self.proto = proto
            self.paused = False

        def pauseProducing(self):
            self.paused = True

        def resumeProducing(self):
            self.paused = False
            proto = self.proto

            # logging.debug("*******sent: {}, ratio: {}".format(proto.sent_size, proto.sent_size/proto.data_size))
            
            if not self.paused and proto.sent_size < proto.data_size:
                # logging.debug("########sent: {}, ratio: {}".format(proto.sent_size, proto.sent_size/proto.data_size))
                try:
                    size_written = proto.transport.write(self.data_block, proto.peer)
                except socket.error as se:
                    if se.errno == errno.EWOULDBLOCK:
                        self.pauseProducing()
                        proto.transport.startWriting()
                        logging.debug("got EWOULDBLOCK.")
                        return
                    else:
                        logging.debug("^^^^^^^^^^^ FATAL.")
                        raise

                proto.sent_size += size_written
                reactor.callLater(0, self.resumeProducing)


            if proto.sent_size >= proto.data_size:
                logging.debug("done transmission, {}: {}".format(proto.sent_size, proto.data_size))
                # important
                proto.transport.stopWriting()

                proto.transport.unregisterProducer()

        def stopProducing(self):
            self.paused = True


    def __init__(self, dst_ip, dst_port, data_size, flow_uid):
        global datahouse_sender
        datahouse_sender = self

        self.peer = dst_ip, dst_port
        self.data_size = data_size  # in bytes
        self.sent_size = 0
        self.flow_uid = flow_uid


    def startProtocol(self):
        logging.debug("sender: start protocol.")

        producer = self.Producer(self) 
        self.transport.registerProducer(producer, True)

        wp_broker.report_remaining_size()

        producer.resumeProducing()

    def connectionRefused(self):
        logging.debug("xxxxx  wrong! connection refused.")
        
        # self.transport.connect(self.dst_ip, self.dst_port)


class DataHouseReceiver(DatagramProtocol):

    def startProtocol(self):
        logging.debug("started.")

    # def datagramReceived(self, datagram, addr):
    #     import time
    #     print("received: len: {}, from: {}, time: {}".format(len(datagram), addr, time.time()))
        

class WorkerProcessBroker(NetstringReceiver):
    def __init__(self):
        global wp_broker
        wp_broker = self

        self.token = sys.argv[1]
        self.rate_idx = -1

    def connectionMade(self):
        status = zebra_pb2.Status()
        status.event_type = status.WORKER_REGISTER
        status.worker_register.token = self.token
        self.sendString(status.SerializeToString())


    def connectionLost(self, reason):
        if reactor.running:
            reactor.stop()


    def stringReceived(self, string):
        status = zebra_pb2.Status()
        status.ParseFromString(string)

        if status.event_type is status.LAUNCH:
            logging.debug("launch status received.")

            launch = status.launch
            if launch.is_src:
                src_port = launch.src_port
                dst_port = launch.dst_port
                dst_id = launch.dst_id
                flowsize = launch.flowsize

                # we don't want the rate to be too small
                rate = int(launch.rate)
                rate = max(rate, min_send_rate)  #20kB

                job_id = launch.flow_uid >> 16;  flow_id = launch.flow_uid & ((1<<16)-1)
                class_id = format((job_id+1)*256 + flow_id+1, 'x') #16 0x

                # logging.debug("class_id: {}, port: {}, peer_ip: {}, peer_port: {}, rate: {}".format(class_id, src_port, dst_ip, dst_port, rate))

                dst_ip = hostname_to_ip["slave-{}".format(dst_id)]
                args = map(str, ("init-sender", class_id, src_port, dst_ip, dst_port, rate))

                # if rate is -1, we are actually in raw TCP mode.
                if rate != -1:
                    utils.getProcessOutput(tc_path, args=args, env=os.environ)

                # reactor.listenUDP(src_port, DataHouseSender(dst_ip, dst_port, flowsize, launch.flow_uid))

                # we run block sender in a different thread
                self.sender = BlockSender(src_port, dst_ip, dst_port, flowsize, launch.flow_uid)  # flowsize: in byes
                reactor.callInThread(self.sender.send)
                self.report_remaining_size()

            else:
                pass

                # dst_port = launch.dst_port
                # do we really need this? no need as we are now using UDP.
                # reactor.listenUDP(dst_port, DataHouseReceiver())
            return


        if status.event_type is status.RATELIMIT:
            logging.debug("rate-limit status received.")

            rate_limit = status.rate_limit

            rate_idx = rate_limit.update_id
            if rate_idx < self.rate_idx:
                return
            self.rate_idx = rate_idx

            flow_uid = rate_limit.flow_uid
            job_id = flow_uid >> 16
            flow_id = flow_uid & ((1<<16)-1)

            class_id = format((job_id+1)*256 + flow_id+1, 'x') 

            rate = int(rate_limit.rate)
            rate = max(rate, min_send_rate)

            logging.debug("rate is:{}".format(rate))

            deferred = utils.getProcessOutput(tc_path, args=("update-sender", str(class_id), str(rate)), env=os.environ)


    def report_remaining_size(self):

        def _report_remaining_size():
            status = zebra_pb2.Status()
            status.event_type = status.WORKER_UPDATE
            status.worker_update.flow_uid = self.sender.flow_uid
            status.worker_update.remaining_size = max(0, self.sender.data_size - self.sender.sent_size)

            # logging.debug("running with: {}".format(status.worker_update.remaining_size))

            self.sendString(status.SerializeToString())

            if status.worker_update.remaining_size <= ZERO:
                self.lc.stop()

                self.transport.loseConnection()

                # directly calling ".stop" here will cancel the sending
                # reactor.stop()

        self.lc = task.LoopingCall(_report_remaining_size)
        self.lc.start(0.3)


class WorkerProcessBrokerFactory(ClientFactory):
    protocol = WorkerProcessBroker
    def clientConnectionFailed(self, connector, reason):
        # logging.warn("connection failed.")
        connector.connect()


def main():
    global wpb_factory

    wpb_factory = WorkerProcessBrokerFactory()
    # ephemeral port are bound based upon /proc/sys/net/ipv4/ip_local_port_range
    reactor.connectTCP(b"localhost", 6822, wpb_factory, timeout=0.1)

    reactor.run()
    

if __name__ == '__main__':
    main()
