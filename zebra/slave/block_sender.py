from __future__ import absolute_import, print_function, division

import socket

from zebra.helper import logging

class BlockSender(object):

    data_block_size = 1024
    data_block = 'H'*data_block_size     # 'HHHHHHHHHHH'

    def __init__(self, src_port, dst_ip, dst_port, data_size, flow_uid):
        self.peer = dst_ip, dst_port
        self.data_size = data_size  # in bytes
        self.sent_size = 0
        self.flow_uid = flow_uid

        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)  # UDP
        self.sock.bind(("", src_port))


    def send(self):
        logging.debug("block-sender is now sending.")
        while self.sent_size < self.data_size:
            size_written = self.sock.sendto(self.data_block, self.peer)    #  Return the number of bytes sent.
            self.sent_size += size_written
            # logging.debug("sent: {}".format(self.sent_size))



if __name__ == '__main__':

    block_sender = BlockSender(8383, "32.41.32.1", 5001, float("inf"),  "dummy")
    block_sender.send()
