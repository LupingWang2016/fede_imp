#!/usr/bin/env python
from __future__ import absolute_import, print_function, division
# import pyximport; pyximport.install()
import sys
sys.path.append("/Users/ourokutaira/Desktop/edge/implementation/zebra/")
from twisted.internet import reactor, endpoints, utils
from twisted.internet.protocol import ServerFactory, ClientFactory, ProcessProtocol
from twisted.protocols.basic import NetstringReceiver
from Slave_scheduler import Slave_Scheduler
from feder import zebra_pb2
from helper import logging
from feder.Helper_Functions import putIntoProto_master,extractOutProto_master,putIntoProto_slave,extractOutProto_slave
from feder.MNIST_reader import Data,Data_word,Data_word_2
from feder.DiffPrivate_FedLearning_word import PrivAgent,Flag,check_validaity_of_FLAGS,global_step_creator,Vname_to_FeedPname,\
    load_from_directory_or_initialize,Vname_to_Pname,bring_Accountant_up_to_date,print_loss_and_accuracy,save_progress,\
    print_new_comm_round,create_save_dir
import os
import tensorflow as tf
import feder.mnist_inference as mnist
from feder.DiffPrivate_FedLearning_word import run_differentially_private_federated_averaging
from feder.MNIST_reader import Data,Data_word,Data_word_2
import argparse
import sys
import nltk
nltk.download('punkt')

pwd = os.path.dirname(os.path.abspath(__file__))
tc_path = os.path.join(pwd, "tc.sh")
worker_path = os.path.join(pwd, "worker.py")


class Broker(NetstringReceiver):

    def __init__(self):
        global slave_broker
        slave_broker = self
        self.MAX_LENGTH=99999999


    def connectionMade(self):
        logging.info("connnected to master.")
        status = zebra_pb2.Status()
        status.event_type = status.REGISTER
        self.sendString(status.SerializeToString())


    def connectionLost(self, reason):
        logging.info("slave connection lost.")
        logging.info(reason)
        reactor.stop()


    def stringReceived(self, string):
        status = zebra_pb2.Status()
        status.ParseFromString(string)

        if status.event_type is status.LAUNCH:
            self.launch_flow(status)
            return 

        if status.event_type is status.RATELIMIT:

            self.rate_limit_flow(status)
            return

        logging.error("wrong event type.")


    def get_token(self, flow_uid, is_src):
        return "{}:{}".format(flow_uid, int(is_src))


    def rate_limit_flow(self, status):
        # rate_limit = status.rate_limit
        New_weights_return,Sanitized_Updates= extractOutProto_master(sched.oldWeight, status)

        ifSig = sched.startNewRoundTraining(New_weights=New_weights_return,Sanitized_Updates=Sanitized_Updates)
        logging.info("new round.")
        #check if sig
        self.update_worker(ifSig)
        # logging.info("update worker.")



    def launch_flow(self, status):  # a flow : a worker
        launch = status.launch
        self.worker_id=launch.worker_id
        sched.startSlave(worker_id=self.worker_id)
        logging.info("launch worker.")
        ifSig=1;
        self.update_worker(ifSig)
        logging.info("update worker.")

    def update_worker(self,ifSig):

        New_weights_status = putIntoProto_slave(sched.oldWeight,sched.worker_id,ifSig)
        # real_round, my_id, my_ifSig, New_weights = extractOutProto_slave(sched.oldWeight, New_weights_status)
        my_test_string = New_weights_status.SerializeToString()
        # print(len(my_test_string))
        # print(sys.getsizeof(my_test_string))
        # print ("This round's weight for update is "+str(sched.oldWeight[0][0,0]))
        # self._extractLength(my_test_string)
        self.sendString(New_weights_status.SerializeToString())
        # print(New_weights_status.SerializeToString())

        #for debug

        # status = zebra_pb2.Status()
        # status.event_type = status.WORKER_UPDATE
        # worker_update = status.worker_update
        # worker_update.my_id = 1
        # print(status.SerializeToString())
        # # self.sendString(status.SerializeToString())




class BrokerFactory(ClientFactory):
    protocol = Broker
    # keep connecting.
    def clientConnectionFailed(self, connector, reason):
        connector.connect()


class WorkerProcessBroker(NetstringReceiver):

    def stringReceived(self, string):
        status = zebra_pb2.Status()
        status.ParseFromString(string)

        if status.event_type is status.WORKER_REGISTER:
            token = status.worker_register.token
            wpb_factory.workers[token] = self

            # send pending events
            for s in wpb_factory.pending_status.setdefault(token, []):
                self.sendString(s.SerializeToString())                     # send to the worker! NOT the master

            wpb_factory.pending_status[token] = []

            logging.info("worker registered with token: {}".format(token))
            return

        if status.event_type is status.WORKER_UPDATE:
            slave_broker.sendString(status.SerializeToString())
            return

        logging.error("unsupported event type.")
        

class WorkerProcessBrokerFactory(ServerFactory):
    protocol = WorkerProcessBroker

    def __init__(self):
        # token -> broker
        self.workers = {}
        self.pending_status = {}



def serve():
    global wpb_factory
    
    # endpoints.clientFromString(reactor, "tcp:master:7823").connect(BrokerFactory())
    # reactor.connectTCP(b"master", 7823, BrokerFactory(), timeout=0.5, bindAddress=(b"0.0.0.0", 6823))
    reactor.connectTCP(b"master", 7824, BrokerFactory(), timeout=0.5)

    wpb_factory = WorkerProcessBrokerFactory()
    endpoints.serverFromString(reactor, "tcp:6822").listen(wpb_factory)
        


def main(_):
    # we first reset the tc on this host

    use_signi = True
    threshold = 0.6011
    methodSig = 1

    global sched
    sched = Slave_Scheduler(N=FLAGS.host_n, b=FLAGS.b, e=FLAGS.e,m=FLAGS.m, sigma=FLAGS.sigma, eps=FLAGS.eps, save_dir=os.getcwd(), log_dir=FLAGS.log_dir, \
            use_signi=use_signi,threshold=threshold,methodSig=methodSig)

    deferred = utils.getProcessOutput(tc_path, args=("reset", ), env=os.environ)
    deferred.addErrback(lambda _: logging.error("error on tc reset."))
    deferred.addCallback(lambda _: serve())

    
    reactor.run()


if __name__ == '__main__':
    # main()
    parser = argparse.ArgumentParser()
    # parser.add_argument(
    #     '--save_dir',
    #     type=str,
    #     default=os.getcwd(),
    #     help='directory to store progress'
    # )
    parser.add_argument(
        '--N',
        type=int,
        default=100,
        help='Total Number of clients participating'
    )
    parser.add_argument(
        '--sigma',
        type=float,
        default=0,
        help='The gm variance parameter; will not affect if Priv_agent is set to True'
    )
    parser.add_argument(
        '--eps',
        type=float,
        default=8,
        help='Epsilon'
    )
    parser.add_argument(
        '--m',
        type=int,
        default=0,
        help='Number of clients participating in a round'
    )
    parser.add_argument(
        '--b',
        type=float,
        default=2,
        help='Batches per client'
    )
    parser.add_argument(
        '--e',
        type=int,
        default=5,
        help='Epochs per client'
    )
    parser.add_argument(
        '--save_dir',
        type=str,
        default=os.getcwd(),
        help='Directory'
    )
    parser.add_argument(
        '--log_dir',
        type=str,
        default=os.path.join(os.getenv('TEST_TMPDIR', '/tmp'),
                             'tensorflow/mnist/logs/fully_connected_feed'),
        help='Directory to put the log data.'
    )
    parser.add_argument(
        '--host_n',
        type=int,
        default=100,
        help='Number of servers'
    )
    FLAGS, unparsed = parser.parse_known_args()
    tf.app.run(main=main, argv=[sys.argv[0]] + unparsed)
