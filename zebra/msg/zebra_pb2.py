# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: zebra.proto

import sys
_b=sys.version_info[0]<3 and (lambda x:x) or (lambda x:x.encode('latin1'))
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
from google.protobuf import descriptor_pb2
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()




DESCRIPTOR = _descriptor.FileDescriptor(
  name='zebra.proto',
  package='zebra',
  syntax='proto2',
  serialized_pb=_b('\n\x0bzebra.proto\x12\x05zebra\"\x1b\n\nmetaMatrix\x12\r\n\x05value\x18\x01 \x03(\x02\"\xa2\x01\n\x0cWeightMatrix\x12#\n\x08weight_0\x18\x01 \x02(\x0b\x32\x11.zebra.metaMatrix\x12#\n\x08weight_1\x18\x02 \x02(\x0b\x32\x11.zebra.metaMatrix\x12#\n\x08weight_2\x18\x03 \x02(\x0b\x32\x11.zebra.metaMatrix\x12#\n\x08weight_3\x18\x04 \x02(\x0b\x32\x11.zebra.metaMatrix\"\xf2\x04\n\x06Status\x12+\n\nevent_type\x18\x01 \x02(\x0e\x32\x17.zebra.Status.EventType\x12$\n\x06launch\x18\x02 \x01(\x0b\x32\x14.zebra.Status.Launch\x12+\n\nrate_limit\x18\x03 \x01(\x0b\x32\x17.zebra.Status.RateLimit\x12\x35\n\x0fworker_register\x18\x04 \x01(\x0b\x32\x1c.zebra.Status.WorkerRegister\x12\x31\n\rworker_update\x18\x05 \x01(\x0b\x32\x1a.zebra.Status.WorkerUpdate\x1a\x8e\x01\n\x06Launch\x12\x10\n\x08\x66low_uid\x18\x01 \x02(\x05\x12\x0e\n\x06is_src\x18\x02 \x02(\x08\x12\x0e\n\x06src_id\x18\x03 \x02(\x05\x12\x0e\n\x06\x64st_id\x18\x04 \x02(\x05\x12\x10\n\x08src_port\x18\x05 \x02(\x05\x12\x10\n\x08\x64st_port\x18\x06 \x02(\x05\x12\x10\n\x08\x66lowsize\x18\x07 \x02(\x05\x12\x0c\n\x04rate\x18\x08 \x02(\x01\x1a\x36\n\tRateLimit\x12)\n\x0cweightmatrix\x18\x01 \x02(\x0b\x32\x13.zebra.WeightMatrix\x1a\x1f\n\x0eWorkerRegister\x12\r\n\x05token\x18\x01 \x02(\t\x1a\x36\n\x0cWorkerUpdate\x12&\n\tmy_weight\x18\x01 \x02(\x0b\x32\x13.zebra.WeightMatrix\"\\\n\tEventType\x12\x0c\n\x08REGISTER\x10\x00\x12\n\n\x06LAUNCH\x10\x01\x12\r\n\tRATELIMIT\x10\x02\x12\x13\n\x0fWORKER_REGISTER\x10\x03\x12\x11\n\rWORKER_UPDATE\x10\x04')
)



_STATUS_EVENTTYPE = _descriptor.EnumDescriptor(
  name='EventType',
  full_name='zebra.Status.EventType',
  filename=None,
  file=DESCRIPTOR,
  values=[
    _descriptor.EnumValueDescriptor(
      name='REGISTER', index=0, number=0,
      options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='LAUNCH', index=1, number=1,
      options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='RATELIMIT', index=2, number=2,
      options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='WORKER_REGISTER', index=3, number=3,
      options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='WORKER_UPDATE', index=4, number=4,
      options=None,
      type=None),
  ],
  containing_type=None,
  options=None,
  serialized_start=751,
  serialized_end=843,
)
_sym_db.RegisterEnumDescriptor(_STATUS_EVENTTYPE)


_METAMATRIX = _descriptor.Descriptor(
  name='metaMatrix',
  full_name='zebra.metaMatrix',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='value', full_name='zebra.metaMatrix.value', index=0,
      number=1, type=2, cpp_type=6, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=22,
  serialized_end=49,
)


_WEIGHTMATRIX = _descriptor.Descriptor(
  name='WeightMatrix',
  full_name='zebra.WeightMatrix',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='weight_0', full_name='zebra.WeightMatrix.weight_0', index=0,
      number=1, type=11, cpp_type=10, label=2,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='weight_1', full_name='zebra.WeightMatrix.weight_1', index=1,
      number=2, type=11, cpp_type=10, label=2,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='weight_2', full_name='zebra.WeightMatrix.weight_2', index=2,
      number=3, type=11, cpp_type=10, label=2,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='weight_3', full_name='zebra.WeightMatrix.weight_3', index=3,
      number=4, type=11, cpp_type=10, label=2,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=52,
  serialized_end=214,
)


_STATUS_LAUNCH = _descriptor.Descriptor(
  name='Launch',
  full_name='zebra.Status.Launch',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='flow_uid', full_name='zebra.Status.Launch.flow_uid', index=0,
      number=1, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='is_src', full_name='zebra.Status.Launch.is_src', index=1,
      number=2, type=8, cpp_type=7, label=2,
      has_default_value=False, default_value=False,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='src_id', full_name='zebra.Status.Launch.src_id', index=2,
      number=3, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='dst_id', full_name='zebra.Status.Launch.dst_id', index=3,
      number=4, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='src_port', full_name='zebra.Status.Launch.src_port', index=4,
      number=5, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='dst_port', full_name='zebra.Status.Launch.dst_port', index=5,
      number=6, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='flowsize', full_name='zebra.Status.Launch.flowsize', index=6,
      number=7, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='rate', full_name='zebra.Status.Launch.rate', index=7,
      number=8, type=1, cpp_type=5, label=2,
      has_default_value=False, default_value=float(0),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=462,
  serialized_end=604,
)

_STATUS_RATELIMIT = _descriptor.Descriptor(
  name='RateLimit',
  full_name='zebra.Status.RateLimit',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='weightmatrix', full_name='zebra.Status.RateLimit.weightmatrix', index=0,
      number=1, type=11, cpp_type=10, label=2,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=606,
  serialized_end=660,
)

_STATUS_WORKERREGISTER = _descriptor.Descriptor(
  name='WorkerRegister',
  full_name='zebra.Status.WorkerRegister',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='token', full_name='zebra.Status.WorkerRegister.token', index=0,
      number=1, type=9, cpp_type=9, label=2,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=662,
  serialized_end=693,
)

_STATUS_WORKERUPDATE = _descriptor.Descriptor(
  name='WorkerUpdate',
  full_name='zebra.Status.WorkerUpdate',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='my_weight', full_name='zebra.Status.WorkerUpdate.my_weight', index=0,
      number=1, type=11, cpp_type=10, label=2,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=695,
  serialized_end=749,
)

_STATUS = _descriptor.Descriptor(
  name='Status',
  full_name='zebra.Status',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='event_type', full_name='zebra.Status.event_type', index=0,
      number=1, type=14, cpp_type=8, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='launch', full_name='zebra.Status.launch', index=1,
      number=2, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='rate_limit', full_name='zebra.Status.rate_limit', index=2,
      number=3, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='worker_register', full_name='zebra.Status.worker_register', index=3,
      number=4, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='worker_update', full_name='zebra.Status.worker_update', index=4,
      number=5, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
  ],
  extensions=[
  ],
  nested_types=[_STATUS_LAUNCH, _STATUS_RATELIMIT, _STATUS_WORKERREGISTER, _STATUS_WORKERUPDATE, ],
  enum_types=[
    _STATUS_EVENTTYPE,
  ],
  options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=217,
  serialized_end=843,
)

_WEIGHTMATRIX.fields_by_name['weight_0'].message_type = _METAMATRIX
_WEIGHTMATRIX.fields_by_name['weight_1'].message_type = _METAMATRIX
_WEIGHTMATRIX.fields_by_name['weight_2'].message_type = _METAMATRIX
_WEIGHTMATRIX.fields_by_name['weight_3'].message_type = _METAMATRIX
_STATUS_LAUNCH.containing_type = _STATUS
_STATUS_RATELIMIT.fields_by_name['weightmatrix'].message_type = _WEIGHTMATRIX
_STATUS_RATELIMIT.containing_type = _STATUS
_STATUS_WORKERREGISTER.containing_type = _STATUS
_STATUS_WORKERUPDATE.fields_by_name['my_weight'].message_type = _WEIGHTMATRIX
_STATUS_WORKERUPDATE.containing_type = _STATUS
_STATUS.fields_by_name['event_type'].enum_type = _STATUS_EVENTTYPE
_STATUS.fields_by_name['launch'].message_type = _STATUS_LAUNCH
_STATUS.fields_by_name['rate_limit'].message_type = _STATUS_RATELIMIT
_STATUS.fields_by_name['worker_register'].message_type = _STATUS_WORKERREGISTER
_STATUS.fields_by_name['worker_update'].message_type = _STATUS_WORKERUPDATE
_STATUS_EVENTTYPE.containing_type = _STATUS
DESCRIPTOR.message_types_by_name['metaMatrix'] = _METAMATRIX
DESCRIPTOR.message_types_by_name['WeightMatrix'] = _WEIGHTMATRIX
DESCRIPTOR.message_types_by_name['Status'] = _STATUS
_sym_db.RegisterFileDescriptor(DESCRIPTOR)

metaMatrix = _reflection.GeneratedProtocolMessageType('metaMatrix', (_message.Message,), dict(
  DESCRIPTOR = _METAMATRIX,
  __module__ = 'zebra_pb2'
  # @@protoc_insertion_point(class_scope:zebra.metaMatrix)
  ))
_sym_db.RegisterMessage(metaMatrix)

WeightMatrix = _reflection.GeneratedProtocolMessageType('WeightMatrix', (_message.Message,), dict(
  DESCRIPTOR = _WEIGHTMATRIX,
  __module__ = 'zebra_pb2'
  # @@protoc_insertion_point(class_scope:zebra.WeightMatrix)
  ))
_sym_db.RegisterMessage(WeightMatrix)

Status = _reflection.GeneratedProtocolMessageType('Status', (_message.Message,), dict(

  Launch = _reflection.GeneratedProtocolMessageType('Launch', (_message.Message,), dict(
    DESCRIPTOR = _STATUS_LAUNCH,
    __module__ = 'zebra_pb2'
    # @@protoc_insertion_point(class_scope:zebra.Status.Launch)
    ))
  ,

  RateLimit = _reflection.GeneratedProtocolMessageType('RateLimit', (_message.Message,), dict(
    DESCRIPTOR = _STATUS_RATELIMIT,
    __module__ = 'zebra_pb2'
    # @@protoc_insertion_point(class_scope:zebra.Status.RateLimit)
    ))
  ,

  WorkerRegister = _reflection.GeneratedProtocolMessageType('WorkerRegister', (_message.Message,), dict(
    DESCRIPTOR = _STATUS_WORKERREGISTER,
    __module__ = 'zebra_pb2'
    # @@protoc_insertion_point(class_scope:zebra.Status.WorkerRegister)
    ))
  ,

  WorkerUpdate = _reflection.GeneratedProtocolMessageType('WorkerUpdate', (_message.Message,), dict(
    DESCRIPTOR = _STATUS_WORKERUPDATE,
    __module__ = 'zebra_pb2'
    # @@protoc_insertion_point(class_scope:zebra.Status.WorkerUpdate)
    ))
  ,
  DESCRIPTOR = _STATUS,
  __module__ = 'zebra_pb2'
  # @@protoc_insertion_point(class_scope:zebra.Status)
  ))
_sym_db.RegisterMessage(Status)
_sym_db.RegisterMessage(Status.Launch)
_sym_db.RegisterMessage(Status.RateLimit)
_sym_db.RegisterMessage(Status.WorkerRegister)
_sym_db.RegisterMessage(Status.WorkerUpdate)


# @@protoc_insertion_point(module_scope)
