#!/usr/bin/env bash

# XXX for the root user part
apt-get update
(pass=aakb1909 ; printf "$pass\n$pass\n" | adduser --gecos "" introom >/dev/null 2>&1)
printf "\nintroom ALL=(ALL:ALL) NOPASSWD:ALL" >> /etc/sudoers

# XXX on the home side
# ssh-keygen -f "/home/introom/.ssh/known_hosts" -R worker0
ssh-copy-id introom@slave

# XXX normal introom user

# directories
cd ; mkdir -p work data temp

sudo apt-get install -y rsync python-protobuf python-twisted iperf

# install nethogs from jessie-backports
# sudo aptitude -t jessie-backports install nethogs


# clear clear
sudo apt-get clean ; sudo rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* ; rm -rf ~/temp/*
