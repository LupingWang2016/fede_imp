#!/usr/bin/env python2
from __future__ import absolute_import, print_function, division

# XXX this module will update the hosts file based upon query to linode
# only supports basic ini format

from collections import namedtuple
import itertools
import operator
from cStringIO import StringIO
import os.path as osp
from linode import api as linode_api


def update_hostsfile(api_keys, inventory_path):

    hosts = {}
    # LINODEID: LPM_DISPLAYGROUP, LABEL, IPADDRESS
    HostInfo = namedtuple("HostInfo", ["display_group", "label", "ipaddress"])

    for api_key in api_keys:
        api = linode_api.Api(api_key)
        for h in api.linode_list():
            linode_id = h['LINODEID']
            display_group = h['LPM_DISPLAYGROUP']
            label = h['LABEL']
            hosts[linode_id] = HostInfo(display_group, label, 0)
        for ip in api.linode_ip_list():
            linode_id = ip['LINODEID']
            ipaddress = ip['IPADDRESS']
            info = hosts[linode_id]
            hosts[linode_id] = HostInfo(info.display_group, info.label, ipaddress)
    # for h in hosts.iteritems():
    #     print(h)

    # store the groups back to inventory file.
    # XXX this is a total overwriting.
    fp_inventory = StringIO()
    hosts_values = sorted(
        hosts.itervalues(), key=operator.attrgetter("display_group"))
    for group, infos in itertools.groupby(
            hosts_values, operator.attrgetter("display_group")):
        infos = tuple(infos)  # infos is transient
        fp_inventory.write("\n\n[{}]\n".format(group))
        labels = "\n".join(info.label for info in
            sorted(infos, key=operator.attrgetter("label")))
        fp_inventory.write(labels)

    with open(inventory_path, "w") as f:
        f.write(fp_inventory.getvalue())
        f.write("\n")
        fp_inventory.close()

    # we update the /etc/hosts
    fp_hosts = StringIO()
    fp_hosts.write("127.0.0.1\tlocalhost\n")
    # damn the Py2 has no nonlocal
    has_master = dict(found=False)
    def key_func_wrapper(ele):
        key_func = operator.attrgetter("label")
        if not has_master["found"] and key_func(ele) == "master":
            has_master["found"] = True
        return key_func

    fp_hosts.write(
            "\n".join(
                "{}\t{}".format(info.ipaddress, info.label) for info in
                    sorted(hosts.itervalues(), key=key_func_wrapper)))

    with open("/etc/hosts", "w") as f:
        f.write(fp_hosts.getvalue())
        f.write("\n")
        fp_hosts.close()
        if not has_master["found"]:
            import socket
            s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            s.connect(("8.8.4.4", 53))
            ip = s.getsockname()[0]
            f.write("{}\tmaster\n".format(ip))


def create_hosts(api_key, plan, datacenter, group, prefix,
                 start_id, num, name, distribution, image, password, swap_size):

    api = linode_api.Api(api_key)

    # kernel_id for profile
    for distro in api.avail_distributions():
        if distro['DISTRIBUTIONID'] == distribution:
            arch = 64 if distro['IS64BIT'] else 32
            break

    for kernel in api.avail_kernels():
        if kernel['LABEL'].startswith("Latest {}".format(arch)):
            kernel_id = kernel['KERNELID']
            break

    def get_labels():
        if name:
            yield name
            return
        else:
            for idx in range(start_id, start_id+num):
                yield "{}{}".format(prefix, idx)
    
    for label in get_labels():
        res = api.linode_create(
                DatacenterID=datacenter, PlanID=plan, PaymentTerm=1)
        linode_id = res['LinodeID']

        # update host info
        api.linode_update(
            LinodeId=linode_id, Label=label, lpm_displayGroup=group)

        info = api.linode_list(LinodeId=linode_id)

        # create disk
        root_size = info[0]['TOTALHD'] - swap_size
        # we do not need that much, and sometimes linode doesn't have that
        # much space.
        root_size = min(root_size, 200000)

        if image:
            for img in api.image_list():
                if img['LABEL'] == image:
                    api.linode_disk_createfromimage(
                            LinodeId=linode_id, ImageID=img['IMAGEID'],
                            rootPass=password, Label="{}-root".format(label),
                            size=root_size)
                    break
            else:
                raise EnvironmentError("no images found.")

        else:
            api.linode_disk_createfromdistribution(
                    LinodeId=linode_id, DistributionID=distribution,
                    rootPass=password, Label="{}-root".format(label),
                    Size=root_size)

        # swap
        api.linode_disk_create(
                LinodeId=linode_id, Type='swap',
                Size=swap_size, Label="{}-swap".format(label))

        # config
        disks = api.linode_disk_list(LinodeId=linode_id)
        disk_ids = [disk['DISKID'] for disk in
            sorted(disks, key=lambda ele: 1 if ele['TYPE'] == 'swap' else 0)]

        while len(disk_ids) < 9:
            disk_ids.append('')
        disk_ids = ','.join(str(i) for i in disk_ids)

        api.linode_config_create(
                LinodeId=linode_id, KernelId=kernel_id,
                Label=label, Disklist=disk_ids)

        # run it
        api.linode_boot(LinodeId=linode_id)


def delete_hosts(api_keys, groups, name):
    for api_key in api_keys:
        api = linode_api.Api(api_key)
        for h in api.linode_list():
            if name:
                if h['LABEL'] == name:
                    api.linode_delete(LinodeId=h['LINODEID'], skipChecks=True)
                    return
            else:
                if h['LPM_DISPLAYGROUP'] in groups:
                    api.linode_delete(LinodeId=h['LINODEID'], skipChecks=True)


def main():
    import random, string
    _password = 'dU23' + ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(8))

    module = AnsibleModule(
        argument_spec = dict(
            api_keys = dict(),
            inventory_path = dict(),
            state = dict(default="set", choices=["set", "created", "deleted"]),
            # for all plans, see this:
            # https://bitbucket.org/snippets/introom/ek5zd
            plan = dict(type="int", default=1),

            # sg
            # https://www.linode.com/api/utility/avail.datacenters
            datacenter = dict(type="int", default=9),

            # I love Debian (8.1)
            distribution = dict(type="int", default=140),
            image = dict(default=""),
            group = dict(),
            prefix = dict(default="slave-"),
            start_id = dict(type="int", default=0),
            num = dict(type="int", default=1),
            name = dict(default=""),
            password = dict(default=_password),
            swap_size = dict(type="int", default=2000),
        )
    )

    api_keys = module.params.get("api_keys").split(", ")
    state = module.params.get("state")

    if state == "set":
        inventory_path = module.params.get("inventory_path")
        update_hostsfile(api_keys, inventory_path)

    elif state == "created":
        plan = module.params.get("plan")
        datacenter = module.params.get("datacenter")
        group = module.params.get("group").split(", ")
        prefix = module.params.get("prefix")
        start_id = module.params.get("start_id")
        num = module.params.get("num")
        name = module.params.get("name")
        distribution = module.params.get("distribution")
        image = module.params.get("image")
        password = module.params.get("password")
        swap_size = module.params.get("swap_size")
        create_hosts(
            api_keys[0], plan, datacenter, group[0], prefix, start_id,
            num, name, distribution, image, password, swap_size)

    elif state == "deleted":
        group = module.params.get("group").split(", ")
        name = module.params.get("name")
        delete_hosts(api_keys, group, name)


    module.exit_json()


from ansible.module_utils.basic import *

if __name__ == '__main__':
    main()
