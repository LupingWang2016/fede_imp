#!/usr/bin/env python
from __future__ import absolute_import, print_function, division

from enum import Enum
import time

class Flow(object):
    def __init__(self, id_, src_id, dst_id, src_port, dst_port, flowsize, arrival_time):
        self.id = id_
        self.src_id = src_id
        self.dst_id = dst_id
        self.src_port = src_port
        self.dst_port = dst_port
        self.total_bytes = flowsize
        self.remaining_bytes = flowsize

        self.arrival_time = arrival_time
        
        self._bps = 0
        @property
        def bps(self):
            return self._bps

        @bps.setter
        def bps(self, value):
            self._bps = value
            import math
            if math.isnan(self._bps):
                import ipdb; ipdb.set_trace()  # for debug



class Job(object):
    def __init__(self, id_, flows, arrival_time):
        self.id = id_
        self.flows = flows
        self.flows_map = {flow.id: flow for flow in self.flows}   #flow.id -> flow
        self.active_flows = flows
        self.arrival_time = arrival_time


    def on_completed(self, sched_launch_time):
        self.finish_time = time.time() - sched_launch_time
