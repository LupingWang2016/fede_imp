from __future__ import absolute_import, print_function, division

import os.path as osp
import random
import operator
from collections import namedtuple

from trace import MB, NUM_RACKS
from trace.data_structure import Job, Flow


class TraceProducer(object):

    trace_dir = osp.join(osp.dirname(osp.abspath(__file__)), "traces")

    def __init__(self, filename, load=True):
        self.filename = filename
        self.jobs = []

        if load:
            self.load_trace()
            
        # self._add_fixtures()
            


    def _add_fixtures(self):
        flows = []
        flow = Flow(0, 0, 2, 23451, 32141, 2**28-1, 2)
        flows.append(flow)
        flow = Flow(1, 1, 3, 23131, 32142, 2**28-1, 2)
        flows.append(flow)
        job = Job(0, flows, 2) #(self, id_, flows, arrival_time):
        self.jobs.append(job)

        flows = []
        flow = Flow(0, 0, 3, 13231, 32131, 2**28-1, 3)
        flows.append(flow)
        flow = Flow(1, 2, 0, 12313, 32132, 2**28-1, 3)
        flows.append(flow)
        job = Job(1, flows, 3)
        self.jobs.append(job)



    def write_trace(self):

        # for each host/rack, we record its available ports(possibly).
        # the port range we allocate shouldn't conflict with the OS's
        # ephemeral port.
        # C.F., /proc/sys/net/ipv4/ip_local_port_range
        # on my system, it's: 32768-60999

        # our range is 10000-19999 for sender
        # 20000-29999 for receiver


        class _Host(object):
            def __init__(self):
                self._src_port = 10000
                self._dst_port = 20000

            @property
            def src_port(self): return self._src_port

            @src_port.setter
            def src_port(self, value):
                if value >= 20000:
                    raise RuntimeError
                self._src_port = value

            @property
            def dst_port(self): return self._dst_port

            @dst_port.setter
            def dst_port(self, value):
                if value >= 30000:
                    raise RuntimeError
                self._dst_port = value

        from collections import defaultdict

        hosts = defaultdict(_Host)

        flow_fmt = "{job_id} {flow_id} {src_id} {dst_id} {src_port} {dst_port} {flowsize} {arrival_time}"


        # def write_trace_hug():
        #     # to show the effectiveness of sebf, 
        #     arrival_times = (0, 20, 40)

        #     job_id = 0
        #     flow_id = 0
        #     arrival_time = arrival_times[job_id]
        #     flowsize = 1000*1024**2
        #     for i in range(NUM_RACKS):
        #         for j in range(NUM_RACKS):
        #             src_id = i
        #             dst_id = j
        #             src_port = hosts[src_id].src_port; hosts[src_id].src_port += 1
        #             dst_port = hosts[dst_id].dst_port; hosts[dst_id].dst_port += 1
        #             file_.write(flow_fmt.format(**locals())+"\n")
        #             flow_id += 1

        #     job_id = 1
        #     flow_id = 0
        #     arrival_time = arrival_times[job_id]
        #     flowsize = 1000*1024**2
        #     for i in range(NUM_RACKS//2):
        #         src_id = i
        #         dst_id = i + NUM_RACKS//2
        #         src_port = hosts[src_id].src_port; hosts[src_id].src_port += 1
        #         dst_port = hosts[dst_id].dst_port; hosts[dst_id].dst_port += 1
        #         file_.write(flow_fmt.format(**locals())+"\n")
        #         flow_id += 1

        #         src_id = i + NUM_RACKS//2
        #         dst_id = i
        #         src_port = hosts[src_id].src_port; hosts[src_id].src_port += 1
        #         dst_port = hosts[dst_id].dst_port; hosts[dst_id].dst_port += 1
        #         file_.write(flow_fmt.format(**locals())+"\n")
        #         flow_id += 1


        #     job_id = 2
        #     flow_id = 0
        #     arrival_time = arrival_times[job_id]
        #     flowsize = 1000*1024**2
        #     # for i in range(NUM_RACKS//4)+range(NUM_RACKS//2, NUM_RACKS//2+NUM_RACKS//4):
        #     for i in range(NUM_RACKS//4)+range(NUM_RACKS//2, NUM_RACKS//2+NUM_RACKS//4):
        #         src_id = i
        #         dst_id = i + NUM_RACKS//4
        #         src_port = hosts[src_id].src_port; hosts[src_id].src_port += 1
        #         dst_port = hosts[dst_id].dst_port; hosts[dst_id].dst_port += 1
        #         file_.write(flow_fmt.format(**locals())+"\n")
        #         flow_id += 1

        #         src_id = i + NUM_RACKS//4
        #         dst_id = i
        #         src_port = hosts[src_id].src_port; hosts[src_id].src_port += 1
        #         dst_port = hosts[dst_id].dst_port; hosts[dst_id].dst_port += 1
        #         file_.write(flow_fmt.format(**locals())+"\n")
        #         flow_id += 1
                
            
        # with open(osp.join(self.trace_dir, self.filename), 'w') as file_:
        #     write_trace_hug()


        def write_trace_hug():

            # to show the effectiveness of sebf, 
            arrival_times = (0, 10, 20)
            flowsize = 50*1024**2

            job_id = 0
            flow_id = 0
            arrival_time = arrival_times[job_id]
            block = NUM_RACKS // 10
            for i in range(NUM_RACKS):
                start_j = i // block * block
                for j in range(start_j, start_j+block):
                    flowsize = random.randint(30,100)*1024**2
                    src_id = i
                    dst_id = j
                    src_port = hosts[src_id].src_port; hosts[src_id].src_port += 1
                    dst_port = hosts[dst_id].dst_port; hosts[dst_id].dst_port += 1
                    file_.write(flow_fmt.format(**locals())+"\n")
                    flow_id += 1

            job_id = 1
            flow_id = 0
            arrival_time = arrival_times[job_id]
            block = NUM_RACKS//2
            for i in range(block):
                flowsize = random.randint(30, 100) * 1024 ** 2
                src_id = i
                dst_id = i + block
                src_port = hosts[src_id].src_port; hosts[src_id].src_port += 1
                dst_port = hosts[dst_id].dst_port; hosts[dst_id].dst_port += 1
                file_.write(flow_fmt.format(**locals())+"\n")
                flow_id += 1

                flowsize = random.randint(30, 100) * 1024 ** 2
                src_id = i + block
                dst_id = i
                src_port = hosts[src_id].src_port; hosts[src_id].src_port += 1
                dst_port = hosts[dst_id].dst_port; hosts[dst_id].dst_port += 1
                file_.write(flow_fmt.format(**locals())+"\n")
                flow_id += 1


            job_id = 2
            flow_id = 0
            arrival_time = arrival_times[job_id]
            block = NUM_RACKS//6
            for i in range(6):
                for j in range(block):
                    flowsize = random.randint(30, 100) * 1024 ** 2
                    src_id = block*i+j
                    dst_id = i
                    src_port = hosts[src_id].src_port; hosts[src_id].src_port += 1
                    dst_port = hosts[dst_id].dst_port; hosts[dst_id].dst_port += 1
                    file_.write(flow_fmt.format(**locals())+"\n")
                    flow_id += 1

                # src_id = i
                # dst_id = i + block
                # src_port = hosts[src_id].src_port; hosts[src_id].src_port += 1
                # dst_port = hosts[dst_id].dst_port; hosts[dst_id].dst_port += 1
                # file_.write(flow_fmt.format(**locals())+"\n")
                # flow_id += 1
                #
                # src_id = i + block
                # dst_id = i
                # src_port = hosts[src_id].src_port; hosts[src_id].src_port += 1
                # dst_port = hosts[dst_id].dst_port; hosts[dst_id].dst_port += 1
                # file_.write(flow_fmt.format(**locals())+"\n")
                # flow_id += 1
                

            # job_id = 2
            # flow_id = 0
            # arrival_time = arrival_times[job_id]
            # flowsize = 50*1024**2
            # block = NUM_RACKS // 20
            # for i in range(NUM_RACKS):
            #     start_j = i // block * block
            #     for j in range(start_j, start_j+block):
            #         src_id = i
            #         dst_id = j
            #         src_port = hosts[src_id].src_port; hosts[src_id].src_port += 1
            #         dst_port = hosts[dst_id].dst_port; hosts[dst_id].dst_port += 1
            #         file_.write(flow_fmt.format(**locals())+"\n")
            #         flow_id += 1


            
        with open(osp.join(self.trace_dir, self.filename), 'w') as file_:
            write_trace_hug()


    _Flow = namedtuple('_Flow', ("job_id", "flow_id", "src_id", "dst_id", "src_port", "dst_port", "flowsize", "arrival_time"))

    def load_trace(self):
        # job_id -> flows(list)
        flows = {}
        with open(osp.join(self.trace_dir, self.filename)) as f:
            for line in map(str.strip, f):
                if line.startswith("#"):
                    continue

                fields = line.split()
                fields[:-1] = map(int, fields[:-1])
                fields[-1] = float(fields[-1])
                
                _flow = self._Flow._make(fields)

                # we get the corresponding `flows' aggregation
                job_flows = flows.setdefault(_flow.job_id, [])
                flow = Flow(id_ = _flow.flow_id,
                            src_id = _flow.src_id,
                            dst_id = _flow.dst_id,
                            src_port = _flow.src_port,
                            dst_port = _flow.dst_port,
                            flowsize = _flow.flowsize,
                            arrival_time = _flow.arrival_time)
                job_flows.append(flow)

        for job_id, job_flows in flows.iteritems():
            arrival_time = min(f.arrival_time for f in job_flows)
            job = Job(job_id, job_flows, arrival_time)
            self.jobs.append(job)

        # sort the jobs
        self.jobs.sort(key=operator.attrgetter('arrival_time'))



def main():
    tp = TraceProducer("trace-synthesis-1020-r100", False)
    tp.write_trace()

if __name__ == '__main__':
    main()
