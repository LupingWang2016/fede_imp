[Within]  Mistress Ford, Mistress Ford! here's
Mistress Page at the door, sweating and blowing and
looking wildly, and would needs speak with you presently.
Our expectation hath this day an end:
The Dauphin, whom of succors we entreated,
Returns us that his powers are yet not ready
To raise so great a siege. Therefore, great king,
We yield our town and lives to thy soft mercy.
Enter our gates; dispose of us and ours;
For we no longer are defensible.
It is well done, and fitting for a princess
Descended of so many royal kings.
Ah, soldier!
Dies
Re-enter DOLABELLA
