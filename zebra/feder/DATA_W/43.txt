It rested in your grace
To unloose this tied-up justice when you pleased:
And it in you more dreadful would have seem'd
Than in Lord Angelo.
Come, poor babe:
I have heard, but not believed,
the spirits o' the dead
May walk again: if such thing be, thy mother
Appear'd to me last night, for ne'er was dream
So like a waking. To me comes a creature,
Sometimes her head on one side, some another;
I never saw a vessel of like sorrow,
So fill'd and so becoming: in pure white robes,
Like very sanctity, she did approach
My cabin where I lay; thrice bow'd before me,
And gasping to begin some speech, her eyes
Became two spouts: the fury spent, anon
Did this break-from her: 'Good Antigonus,
Since fate, against thy better disposition,
Hath made thy person for the thrower-out
Of my poor babe, according to thine oath,
Places remote enough are in Bohemia,
There weep and leave it crying; and, for the babe
Is counted lost for ever, Perdita,
I prithee, call't. For this ungentle business
Put on thee by my lord, thou ne'er shalt see
Thy wife Paulina more.' And so, with shrieks
She melted into air. Affrighted much,
I did in time collect myself and thought
This was so and no slumber. Dreams are toys:
Yet for this once, yea, superstitiously,
I will be squared by this. I do believe
Hermione hath suffer'd death, and that
Apollo would, this being indeed the issue
Of King Polixenes, it should here be laid,
Either for life or death, upon the earth
Of its right father. Blossom, speed thee well!
There lie, and there thy character: there these;
Which may, if fortune please, both breed thee, pretty,
And still rest thine. The storm begins; poor wretch,
That for thy mother's fault art thus exposed
To loss and what may follow! Weep I cannot,
But my heart bleeds; and most accursed am I
To be by oath enjoin'd to this. Farewell!
The day frowns more and more: thou'rt like to have
A lullaby too rough: I never saw
The heavens so dim by day. A savage clamour!
Well may I get aboard! This is the chase:
I am gone for ever.
Exit, pursued by a bear
Enter a Shepherd
This is most certain that I shall deliver:
Mark Antony is every hour in Rome
Expected: since he went from Egypt 'tis
A space for further travel.
