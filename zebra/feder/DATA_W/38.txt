Laud we the gods;
And let our crooked smokes climb to their nostrils
From our blest altars. Publish we this peace
To all our subjects. Set we forward: let
A Roman and a British ensign wave
Friendly together: so through Lud's-town march:
And in the temple of great Jupiter
Our peace we'll ratify; seal it with feasts.
Set on there! Never was a war did cease,
Ere bloody hands were wash'd, with such a peace.
Exeunt
Ay, sir: the other squirrel was stolen from me by
the hangman boys in the market-place: and then I
offered her mine own, who is a dog as big as ten of
yours, and therefore the gift the greater.
[To KING RICHARD III]
The last was I that helped thee to the crown;
The last was I that felt thy tyranny:
O, in the battle think on Buckingham,
And die in terror of thy guiltiness!
Dream on, dream on, of bloody deeds and death:
Fainting, despair; despairing, yield thy breath!
To RICHMOND
I died for hope ere I could lend thee aid:
But cheer thy heart, and be thou not dismay'd:
God and good angel fight on Richmond's side;
And Richard falls in height of all his pride.
The Ghosts vanish
KING RICHARD III starts out of his dream
