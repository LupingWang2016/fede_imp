I had well hoped thou wouldst have denied Beatrice,
that I might have cudgelled thee out of thy single
life, to make thee a double-dealer; which, out of
question, thou wilt be, if my cousin do not look
exceedingly narrowly to thee.
You men of Angiers, open wide your gates,
And let young Arthur, Duke of Bretagne, in,
Who by the hand of France this day hath made
Much work for tears in many an English mother,
Whose sons lie scattered on the bleeding ground;
Many a widow's husband grovelling lies,
Coldly embracing the discolour'd earth;
And victory, with little loss, doth play
Upon the dancing banners of the French,
Who are at hand, triumphantly display'd,
To enter conquerors and to proclaim
Arthur of Bretagne England's king and yours.
Enter English Herald, with trumpet
So, I am free; yet would not so have been,
Durst I have done my will. O Cassius,
Far from this country Pindarus shall run,
Where never Roman shall take note of him.
Exit
Re-enter TITINIUS with MESSALA
