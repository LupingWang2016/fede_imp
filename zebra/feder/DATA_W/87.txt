Madam, you must come to your uncle. Yonder's old
coil at home: it is proved my Lady Hero hath been
falsely accused, the prince and Claudio mightily
abused; and Don John is the author of all, who is
fed and gone. Will you come presently?
O lord archbishop,
Thou hast made me now a man! never, before
This happy child, did I get any thing:
This oracle of comfort has so pleased me,
That when I am in heaven I shall desire
To see what this child does, and praise my Maker.
I thank ye all. To you, my good lord mayor,
And your good brethren, I am much beholding;
I have received much honour by your presence,
And ye shall find me thankful. Lead the way, lords:
Ye must all see the queen, and she must thank ye,
She will be sick else. This day, no man think
Has business at his house; for all shall stay:
This little one shall make it holiday.
Exeunt
EPILOGUE
'Tis ten to one this play can never please
All that are here: some come to take their ease,
And sleep an act or two; but those, we fear,
We have frighted with our trumpets; so, 'tis clear,
They'll say 'tis naught: others, to hear the city
Abused extremely, and to cry 'That's witty!'
Which we have not done neither: that, I fear,
All the expected good we're like to hear
For this play at this time, is only in
The merciful construction of good women;
For such a one we show'd 'em: if they smile,
And say 'twill do, I know, within a while
All the best men are ours; for 'tis ill hap,
If they hold when their ladies bid 'em clap.
Tear him, tear him! Come, brands ho! fire-brands:
to Brutus', to Cassius'; burn all: some to Decius'
house, and some to Casca's; some to Ligarius': away, go!
Exeunt


Shakespeare homepage 
    | Julius Caesar 
    | Act 3, Scene 3
   
Previous scene
    | Next scene
