Here, here comes master Signior Leonato, and the
Sexton too.
Re-enter LEONATO and ANTONIO, with the Sexton
As I live,
If the king blame me for't, I'll lay ye all
By the heels, and suddenly; and on your heads
Clap round fines for neglect: ye are lazy knaves;
And here ye lie baiting of bombards, when
Ye should do service. Hark! the trumpets sound;
They're come already from the christening:
Go, break among the press, and find a way out
To let the troop pass fairly; or I'll find
A Marshalsea shall hold ye play these two months.
Truly, sir, to wear out their shoes, to get myself
into more work. But, indeed, sir, we make holiday,
to see Caesar and to rejoice in his triumph.
