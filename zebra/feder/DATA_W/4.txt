The king's a beggar, now the play is done:
All is well ended, if this suit be won,
That you express content; which we will pay,
With strife to please you, day exceeding day:
Ours be your patience then, and yours our parts;
Your gentle hands lend us, and take our hearts.
Exeunt
Heavens make a star of him! Yet there, my queen,
We'll celebrate their nuptials, and ourselves
Will in that kingdom spend our following days:
Our son and daughter shall in Tyrus reign.
Lord Cerimon, we do our longing stay
To hear the rest untold: sir, lead's the way.
Exeunt
Enter GOWER
O, spare me not, my brother Edward's son,
For that I was his father Edward's son;
That blood already, like the pelican,
Hast thou tapp'd out and drunkenly caroused:
My brother Gloucester, plain well-meaning soul,
Whom fair befal in heaven 'mongst happy souls!
May be a precedent and witness good
That thou respect'st not spilling Edward's blood:
Join with the present sickness that I have;
And thy unkindness be like crooked age,
To crop at once a too long wither'd flower.
Live in thy shame, but die not shame with thee!
These words hereafter thy tormentors be!
Convey me to my bed, then to my grave:
Love they to live that love and honour have.
Exit, borne off by his Attendants
