Tell him there's a post come from my master, with
his horn full of good news: my master will be here
ere morning.
Exit
Prepare we for our marriage--on which day,
My Lord of Burgundy, we'll take your oath,
And all the peers', for surety of our leagues.
Then shall I swear to Kate, and you to me;
And may our oaths well kept and prosperous be!
Sennet. Exeunt
EPILOGUE
Enter Chorus
'Tear him to pieces.' 'Do it presently.' 'He kill'd
my son.' 'My daughter.' 'He killed my cousin
Marcus.' 'He killed my father.'
