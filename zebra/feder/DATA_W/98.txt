Thou art like the harpy,
Which, to betray, dost, with thine angel's face,
Seize with thine eagle's talons.
O cousin, thou art come to set mine eye:
The tackle of my heart is crack'd and burn'd,
And all the shrouds wherewith my life should sail
Are turned to one thread, one little hair:
My heart hath one poor string to stay it by,
Which holds but till thy news be uttered;
And then all this thou seest is but a clod
And module of confounded royalty.
I have received a hurt: follow me, lady.
Turn out that eyeless villain; throw this slave
Upon the dunghill. Regan, I bleed apace:
Untimely comes this hurt: give me your arm.
Exit CORNWALL, led by REGAN
