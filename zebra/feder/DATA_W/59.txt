I went to her in white, and cried 'mum,' and she
cried 'budget,' as Anne and I had appointed; and yet
it was not Anne, but a postmaster's boy.
We are enow yet living in the field
To smother up the English in our throngs,
If any order might be thought upon.
He hath deserved worthily of his country: and his
ascent is not by such easy degrees as those who,
having been supple and courteous to the people,
bonneted, without any further deed to have them at
an into their estimation and report: but he hath so
planted his honours in their eyes, and his actions
in their hearts, that for their tongues to be
silent, and not confess so much, were a kind of
ingrateful injury; to report otherwise, were a
malice, that, giving itself the lie, would pluck
reproof and rebuke from every ear that heard it.
