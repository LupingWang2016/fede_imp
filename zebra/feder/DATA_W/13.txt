I will not eat my word, now thou art mine;
Thy faith my fancy to thee doth combine.
Enter JAQUES DE BOYS
Follow, I pray you.
Exeunt


Shakespeare homepage 
    | The Tempest 
    | Act 3, Scene 3
   
Previous scene
    | Next scene
Give me mine own again; 'twere no good part
To take on me to keep and kill thy heart.
So, now I have mine own again, be gone,
That I might strive to kill it with a groan.
