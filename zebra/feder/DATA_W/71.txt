Enough, enough, my lord; you have enough:
I beg the law, the law, upon his head.
They would have stolen away; they would, Demetrius,
Thereby to have defeated you and me,
You of your wife and me of my consent,
Of my consent that she should be your wife.
Prepare there,
The duke is coming: see the barge be ready;
And fit it with such furniture as suits
The greatness of his person.
Most fair return of greetings and desires.
Upon our first, he sent out to suppress
His nephew's levies; which to him appear'd
To be a preparation 'gainst the Polack;
But, better look'd into, he truly found
It was against your highness: whereat grieved,
That so his sickness, age and impotence
Was falsely borne in hand, sends out arrests
On Fortinbras; which he, in brief, obeys;
Receives rebuke from Norway, and in fine
Makes vow before his uncle never more
To give the assay of arms against your majesty.
Whereon old Norway, overcome with joy,
Gives him three thousand crowns in annual fee,
And his commission to employ those soldiers,
So levied as before, against the Polack:
With an entreaty, herein further shown,
Giving a paper
That it might please you to give quiet pass
Through your dominions for this enterprise,
On such regards of safety and allowance
As therein are set down.
