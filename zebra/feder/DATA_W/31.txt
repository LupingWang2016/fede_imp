The marble pavement closes, he is enter'd
His radiant root. Away! and, to be blest,
Let us with care perform his great behest.
The Apparitions vanish
A goodly medicine for my aching bones! O world!
world! world! thus is the poor agent despised!
O traitors and bawds, how earnestly are you set
a-work, and how ill requited! why should our
endeavour be so loved and the performance so loathed?
what verse for it? what instance for it? Let me see:
Full merrily the humble-bee doth sing,
Till he hath lost his honey and his sting;
And being once subdued in armed tail,
Sweet honey and sweet notes together fail.
Good traders in the flesh, set this in your
painted cloths.
As many as be here of pander's hall,
Your eyes, half out, weep out at Pandar's fall;
Or if you cannot weep, yet give some groans,
Though not for me, yet for your aching bones.
Brethren and sisters of the hold-door trade,
Some two months hence my will shall here be made:
It should be now, but that my fear is this,
Some galled goose of Winchester would hiss:
Till then I'll sweat and seek about for eases,
And at that time bequeathe you my diseases.
Exit
[To KING RICHARD III]
When I was mortal, my anointed body
By thee was punched full of deadly holes
Think on the Tower and me: despair, and die!
Harry the Sixth bids thee despair, and die!
To RICHMOND
Virtuous and holy, be thou conqueror!
Harry, that prophesied thou shouldst be king,
Doth comfort thee in thy sleep: live, and flourish!
Enter the Ghost of CLARENCE
