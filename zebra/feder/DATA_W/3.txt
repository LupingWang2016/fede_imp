Mine eyes smell onions; I shall weep anon:
To PAROLLES
Good Tom Drum, lend me a handkercher: so,
I thank thee: wait on me home, I'll make sport with thee:
Let thy courtesies alone, they are scurvy ones.
I will, my lord.
Beseech you, first go with me to my house,
Where shall be shown you all was found with her;
How she came placed here in the temple;
No needful thing omitted.
No, Bolingbroke: if ever I were traitor,
My name be blotted from the book of life,
And I from heaven banish'd as from hence!
But what thou art, God, thou, and I do know;
And all too soon, I fear, the king shall rue.
Farewell, my liege. Now no way can I stray;
Save back to England, all the world's my way.
Exit
