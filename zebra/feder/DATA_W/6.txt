Neither his daughter, if we judge by manners;
But yet indeed the lesser is his daughter
The other is daughter to the banish'd duke,
And here detain'd by her usurping uncle,
To keep his daughter company; whose loves
Are dearer than the natural bond of sisters.
But I can tell you that of late this duke
Hath ta'en displeasure 'gainst his gentle niece,
Grounded upon no other argument
But that the people praise her for her virtues
And pity her for her good father's sake;
And, on my life, his malice 'gainst the lady
Will suddenly break forth. Sir, fare you well:
Hereafter, in a better world than this,
I shall desire more love and knowledge of you.
Gentlemen, forward to the bridal dinner:
I see a woman may be made a fool,
If she had not a spirit to resist.
Nor near nor farther off, my gracious lord,
Than this weak arm: discomfort guides my tongue
And bids me speak of nothing but despair.
One day too late, I fear me, noble lord,
Hath clouded all thy happy days on earth:
O, call back yesterday, bid time return,
And thou shalt have twelve thousand fighting men!
To-day, to-day, unhappy day, too late,
O'erthrows thy joys, friends, fortune and thy state:
For all the Welshmen, hearing thou wert dead.
Are gone to Bolingbroke, dispersed and fled.
