Ay, and I'll give them him without a fee.
There do I give to you and Jessica,
From the rich Jew, a special deed of gift,
After his death, of all he dies possess'd of.
After 'the duke his father,' with 'the knife,'
He stretch'd him, and, with one hand on his dagger,
Another spread on's breast, mounting his eyes
He did discharge a horrible oath; whose tenor
Was,--were he evil used, he would outgo
His father by as much as a performance
Does an irresolute purpose.
Kill, kill, kill, kill, kill him!
The Conspirators draw, and kill CORIOLANUS: AUFIDIUS stands on his body
