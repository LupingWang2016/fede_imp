I'll make one in a dance, or so; or I will play
On the tabour to the Worthies, and let them dance the hay.
We are enow yet living in the field
To smother up the English in our throngs,
If any order might be thought upon.
It shall be done, my lord.
Exeunt


Shakespeare homepage 
    | Julius Caesar 
    | Act 4, Scene 3
   
Previous scene
    | Next scene
