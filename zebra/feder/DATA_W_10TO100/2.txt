So please you, he is here at the door and importunes
access to you.
If it be so, yet bragless let it be;
Great Hector was a man as good as he.
We hope to find you our friend; and therefore give
you our voices heartily.
