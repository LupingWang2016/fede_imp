No, I warrant your grace, you shall not entreat him
to a second, that have so mightily persuaded him
from a first.
Hence, broker-lackey! ignomy and shame
Pursue thy life, and live aye with thy name!
Exit
He has done nobly, and cannot go without any honest
man's voice.
