It rested in your grace
To unloose this tied-up justice when you pleased:
And it in you more dreadful would have seem'd
Than in Lord Angelo.
I told your grace they would talk anon.
Drum and trumpet, chambers discharged
I held the sword, and he did run on it.
