Thaliard, adieu!
Exit THALIARD
Till Pericles be dead,
My heart can lend no succor to my head.
Exit
Thy woes will make them sharp, and pierce like mine.
Exit
O, where is Romeo? saw you him to-day?
Right glad I am he was not at this fray.
