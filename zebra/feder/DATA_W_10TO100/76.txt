Half-part, mates, half-part.
Come, let's have her aboard suddenly.
Exeunt Pirates with MARINA
Re-enter LEONINE
I doubt not but his friends will fly to us.
Put this in any liquid thing you will,
And drink it off; and, if you had the strength
Of twenty men, it would dispatch you straight.
