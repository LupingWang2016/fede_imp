When in the world I lived, I was the world's
commander,--
The devil take order now! I'll to the throng:
Let life be short; else shame will be too long.
Exeunt
It shall be done, my lord.
Exeunt


Shakespeare homepage 
    | Julius Caesar 
    | Act 4, Scene 3
   
Previous scene
    | Next scene
