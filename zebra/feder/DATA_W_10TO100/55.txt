Tell him there's a post come from my master, with
his horn full of good news: my master will be here
ere morning.
Exit
The woe's to come; the children yet unborn.
Shall feel this day as sharp to them as thorn.
Thou liest, abhorred tyrant; with my sword
I'll prove the lie thou speak'st.
They fight and YOUNG SIWARD is slain
