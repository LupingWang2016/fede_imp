So 'tis reported:
But none of 'em can be found. Stand! who's there?
These be good humours! your honour wins bad humours.
Exeunt all but Boy
Truly, sir, to wear out their shoes, to get myself
into more work. But, indeed, sir, we make holiday,
to see Caesar and to rejoice in his triumph.
