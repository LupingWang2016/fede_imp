Proceed, proceed: we will begin these rites,
As we do trust they'll end, in true delights.
A dance
EPILOGUE
Fear not: the forest is not three leagues off;
If we recover that, we are sure enough.
Exeunt
'Tear him to pieces.' 'Do it presently.' 'He kill'd
my son.' 'My daughter.' 'He killed my cousin
Marcus.' 'He killed my father.'
