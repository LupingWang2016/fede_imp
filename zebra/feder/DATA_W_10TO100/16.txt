Ne'er may I look on day, nor sleep on night,
But she tells to your highness simple truth!
Dwelt by a churchyard: I will tell it softly;
Yond crickets shall not hear it.
I hope we have reformed that indifferently with us,
sir.
