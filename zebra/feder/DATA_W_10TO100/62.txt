Methinks she should not use a long one for such a
Pyramus: I hope she will be brief.
Marry, my uncle Clarence' angry ghost:
My grandam told me he was murdered there.
The town is empty; on the brow o' the sea
Stand ranks of people, and they cry 'A sail!'
