Good, speak to the mariners: fall to't, yarely,
or we run ourselves aground: bestir, bestir.
Exit
Enter Mariners
A rarer spirit never
Did steer humanity: but you, gods, will give us
Some faults to make us men. Caesar is touch'd.
We are not thieves, but men that much do want.
