Here, here comes master Signior Leonato, and the
Sexton too.
Re-enter LEONATO and ANTONIO, with the Sexton
Come, Grey, come, Vaughan, let us all embrace:
And take our leave, until we meet in heaven.
Exeunt
I am no strumpet; but of life as honest
As you that thus abuse me.
