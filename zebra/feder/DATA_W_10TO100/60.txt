He cannot be heard of. Out of doubt he is
transported.
Our fatherless distress was left unmoan'd;
Your widow-dolour likewise be unwept!
Nothing at all: it is a highwrought flood;
I cannot, 'twixt the heaven and the main,
Descry a sail.
