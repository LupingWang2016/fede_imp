Loath to bid farewell, we take our leaves.
Exeunt Knights
Every man's conscience is a thousand swords,
To fight against that bloody homicide.
We shall be short in our provision:
'Tis now near night.
