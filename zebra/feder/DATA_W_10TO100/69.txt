Think not on him till to-morrow:
I'll devise thee brave punishments for him.
Strike up, pipers.
Dance
Exeunt
No, madam, no; I may not leave it so:
I am bound by oath, and therefore pardon me.
Exit
Enter LORD STANLEY
This did I fear, but thought he had no weapon;
For he was great of heart.
