Pardon me, madam:
If I had given you this at over-night,
She might have been o'erta'en; and yet she writes,
Pursuit would be but vain.
Farewell: yet, soft! Hector! take my leave:
Thou dost thyself and all our Troy deceive.
Exit
A murrain on't! I took this for silver.
Alarum continues still afar off
Enter MARCIUS and TITUS LARTIUS with a trumpet
If she, my liege, can make me know this clearly,
I'll love her dearly, ever, ever dearly.
Farewell: the gods with safety stand about thee!
Exeunt severally PRIAM and HECTOR. Alarums
To Coriolanus come all joy and honour!
Flourish of cornets. Exeunt all but SICINIUS and BRUTUS
So please you, he is here at the door and importunes
access to you.
If it be so, yet bragless let it be;
Great Hector was a man as good as he.
We hope to find you our friend; and therefore give
you our voices heartily.
