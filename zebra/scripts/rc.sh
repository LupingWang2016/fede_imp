#!/user/bin/env zsh

zebra_rcpath="${0:a}"
alias zebra_resource="source $zebra_rcpath"

zebra_rootdir="${zebra_rcpath:h}/../"
# canonical path
zebra_rootdir="${zebra_rootdir:a}"


export PYTHONPATH="$zebra_rootdir/../:$PYTHONPATH"


zebra_run_main="python ${zebra_rootdir}/master/master.py"

# run list
true "
zebra-run HUG  trace-synthesis
zebra-run DRF  trace-synthesis
zebra-run SEBF trace-synthesis 0.5
zebra-run SEBF trace-synthesis 0
"

zebra-run () {
    setopt local_options sh_word_split

    # $1: sub_sharing_algo
    # $2: trace_name
    # $3: fairness
    # $4: relax_scale
    ${zebra_run_main} "$@"
}

enable_cython () {
    mv ${zebra_rootdir}/master/scheduler.py ${zebra_rootdir}/master/scheduler.pyx
}

disable_cython () {
    mv ${zebra_rootdir}/master/scheduler.pyx ${zebra_rootdir}/master/scheduler.py
}

# sync_to_kiwi () {
#     rsync -a $rootdir --exclude="log/" --exclude="plot/" introom@kiwi:./work/
    
# }



# # run sebf with the origin coflow
# # mvn exec:java -Dexec.mainClass="coflowsim.CoflowSim" -Dexec.args="SEBF COFLOW-BENCHMARK benchmark/FB2010-1Hr-150-0.txt"

# sync_origin_to_kiwi () {
#     rsync -a /Users/Eddie/Work/infocom/coflowsim-origin introom@kiwi:./temp/
    
# }
